import Vue from 'vue';
import VueI18n from 'vue-i18n';
import messages from './messages';

function transformToI18nFormat(map) {
    const result = {en: {}, pl: {}, no: {}};
    for (const key in map) {
        if (map.hasOwnProperty(key)) {
            const translations = map[key];
            result.en[key] = translations.en || key;
            result.pl[key] = translations.pl;
            result.no[key] = translations.no;
        }
    }
    return result;
}

window.missingKeys = {};
Vue.use(VueI18n);
export default new VueI18n({
    locale: 'no',
    fallbackLocale: 'en',
    silentTranslationWarn: true,
    messages: transformToI18nFormat(messages),
    missing: (locale, key) => {
        if (process.env.NODE_ENV === 'development') {
            console.error(key);
        }
        window.missingKeys[key] = {pl: key};
        return key;
    }
});
