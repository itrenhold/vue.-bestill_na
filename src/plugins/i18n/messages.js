export default {
    "Application version": {pl: "Wersja aplikacji"},
    "Privacy Policy": {no: "Personvernerklæring", pl: "Polityka Prywatności"},
    "Contact Us": {no: "Kontakt Oss", pl: "Skontaktuj się z nami"},
    "Eastern Norway": {no: "Østlandet", pl: "Wschodnia Norwegia"},
    "Work for us": {no: "Jobb hos oss", pl: "Oferty pracy"},
    "Approved": {no: "Godkjent", pl: "Potwierdzone"},
    "Terms and Conditions": {no: "Avtalevilkår", pl: "Regulamin"},
    "All rights reserved": {
        no: "Alle rettigheter reservert",
        pl: "Wszystkie prawa zasterzone"
    },
    "You are safe with us": {
        pl: "Jesteś bezpieczny",
        no: "Du er trygg med oss"
    },
    "We are insured in": {
        pl: "Jesteśmy ubezpieczeni w",
        no: "Vi er forsikret i"
    },
    "key.footer.newsletter.signup": {
        no: "Meld deg på nyhetsbrevet vårt og få 10% rabatt på ditt første kjøp. Du blir også en av de første som får vite om fordeler og kampanjetilbud fra oss.",
        en: "Sign up for our newsletter and get a 10% discount on your first purchase. You also become one of the first to get to know about benefits and promotional offers from us.",
        pl: "Zapisz się na nasz newsletter i otrzymaj aż 10% zniżki na pierwszy zakup. Będziesz od razu powiadamiany o benefitach i promocjach."
    },
    "key.footer.discount.note": {
        no: "Rabattkoden i mailen er gyldig i 10 dager. Neste gang du bestille hos oss kan du bruke koden når du betaler. Vi ønsker deg en fin dag videre!",
        en: "The discount code in the email is valid for 10 days. The next time you book with us you can use the code when you pay. We wish you a nice day!",
        pl: "Kod rabatowy w wiadomości e-mail jest ważny przez 10 dni. Następnym razem, gdy rezerwujesz u nas, możesz użyć kodu, gdy płacisz. Życzymy miłego dnia!",
    },
    "key.discount.email": {
        en: "You entered an incorrect email address - make sure it doesn't contain any typos.",
        pl: "Podałeś niepoprawny adres e-mail - upewnij się że nie zawiera żadnych literówek.",
        no: "Du har skrevet inn en feil e-postadresse - sørg for at den ikke inneholder noen skrivefeil."
    },
    "key.discount.email.duplicated": {
        en: "You've already received 10% discount code on this email. Check your mailbox :)",
        pl: "Na ten e-mail został już wysłany kod rabatowy na 10%. Sprawdź swoją skrzynkę pocztową :)",
        no: "Du har allerede mottatt 10% rabattkode på denne e-postadressen. Sjekk postkassen din :)"
    },
    "You have a mail!": {
        no: "Du har fått en mail!",
        pl: "Masz wiadomość!",
    },
    "Your e-mail address": {
        no: "Din e-postadresse",
        pl: "Twój adres e-mail"
    },
    "Subscribe": {
        no: "Abonner",
        pl: "Dołącz"
    },
    "100% Secure payment": {
        no: "100% Sikker betaling",
        pl: "100% Bezpieczna płatność",
    },
    "Pay safely and easily with a bank card on the website or invoice.": {
        no: "Betal trygt og enkelt med bankkort via nettsiden eller tilsendt faktura.",
        pl: "Płać bezpiecznie i łatwo kartą bankową na stronie lub na porzez fakturę."
    },
    "High quality": {
        no: "Høy kvalitet",
        pl: "Wysoka jakość"
    },
    "Let us take care of the cleaning for you! Safe, easy and affordable!": {
        no: "La oss ta rengjøringen for deg! Trygt, enkelt og rimelig!",
        pl: "Pozwól nam zająć się sprzątaniem za Ciebie! Bezpieczne, łatwe i niedrogie!"
    },
    "Non-binding": {
        no: "Ingen bindingstid",
        pl: "Bez zobowiązania"
    },
    "We do not have a binding contract, you can order cleaning with us whenever you want.": {
        no: "Vi har ingen bindingstid, hos oss kan du bestille renhold når du vil.",
        pl: "Nie mamy wiążącej umowy, u nas możesz zamówić sprzątanie, kiedy tylko chcesz."
    },
    "Credit Cards": {
        no: "Kredittkort",
        pl: "Karty kredytowe"
    },
    "Home": {no: "Forside", pl: "Główna"},
    "Mobile App": {no: "Mobilapp", pl: "Aplikacja"},
    "Private": {no: "Privat", pl: "Prywatne"},
    "Company": {no: "Bedrift", pl: "Firma"},
    "About us": {no: "Om Oss", pl: "O nas"},
    "Blog": {no: "Blog", pl: "Blog"},
    "Contact": {no: "Kontakt", pl: "Kontakt"},
    "Order now": {no: "Bestill nå", pl: "Zamów teraz"},
    "Enter location": {no: "Oppgi sted", pl: "Podaj lokalizację"},
    "Select service": {no: "Velg tjeneste", pl: "Wybierz usługę"},
    "Determine the time": {no: "Bestem tidspunkt", pl: "Ustal termin"},
    "Select the size of the carpet": {no: "Velg størrelsen på teppet", pl: "Wybierz rozmiar dywanu"},
    "Furniture Cleaning": {pl: "Czyszczenie mebli", no: "Møbler Rengjøring"},
    "Complete the order": {no: "Fullfør bestillingen", pl: "Zakończ zamówienie"},
    "Enter address / location": {no: "Oppgi adresse / sted", pl: "Wprowadź adres / lokalizację"},
    "Where are we going?": {no: "Hvor skal vi komme?", pl: "Dokąd jedziemy?"},
    'We\'re not providing services for the country you\'ve chosen: "{country}".': {
        pl: 'Nie możemy świadczyć usług w kraju, który wybrałeś: "{country}".',
        no: 'Vi tilbyr ikke tjenester for landet du har valgt: "{country}".'
    },
    "Unrecognized": {pl: "Nie rozpoznano", no: "Ukjent"},

    "key.origin.cleared": {
        pl: "Tej lokacji nie można przypisać państwa",
        en: "This location can't be assigned a country",
        no: "Denne plasseringen kan ikke tilordnes et land"
    },
    "key.origin.default": {
        pl: "Nie byliśmy w stanie ustalić Twojej lokacji",
        en: "We were unable to establish your location",
        no: "Vi var ikke i stand til å etablere beliggenheten din"
    },
    "key.origin.geocode": {
        pl: "Twoja lokacja została ustalona przez GPS",
        en: "Your location is established by GPS",
        no: "Din posisjon er etablert av GPS"
    },
    "key.origin.localStorage": {
        pl: "Twoja lokalizacja jest przywrócona z poprzedniego zamówienia",
        en: "Your location is restored from your previous order",
        no: "Din posisjon er gjenopprettet fra din forrige bestilling"
    },
    "key.google.autocomplete.insufficient": {
        pl: "Nie można rozpoznać lokacji",
        en: "Could not recognize location",
        no: "Kunne ikke gjenkjenne plassering",
    },

    "What service should we provide?": {no: "Hvilken tjeneste skal vi utføre?", pl: "Jaką usługę mamy świadczyć?"},
    "Choose suitable time": {no: "Bestem tidspunkt", pl: "Wybierz godzinę"},
    "Please, wait... We are now looking for free hours for you.": {
        no: "Vennligst Vent... Søker vi nå ledig timer for deg.",
        pl: "Proszę czekać... Szukamy dla Ciebie wolnych terminów."
    },
    "To see the available dates, fill in the address in the first step.": {
        no: "For å se de tilgjengelige datoene fyll ut adressen i første steg.",
        pl: "Wpisz adres w pierwszy kroku, by wyświetlić dostępne godziny. "
    },
    "To see the available dates, fill in the equipment details in the first step.": {
        no: "For å se tilgjengelige datoer, fyll ut utstyrsdetaljene i første trinn.",
        pl: "Aby zobaczyć dostępne terminy, podaj dane sprzętu w pierwszym kroku."
    },
    "Go to the first step": {
        no: "Gå til det første trinnet",
        pl: "Wróć do kroku pierwszego"
    },
    "Swipe": {
        pl: "Przesuń",
        no: "Sveip"
    },
    "Summary": {no: "Oppsummering", pl: "Podsumowanie"},
    "Date and time of cleaning": {no: "Dato og klokkeslett for rengjøring", pl: "Data i godzina sprzątania"},
    "Cleaning time": {no: "Rengjøringstid", pl: "Czas sprzątania"},
    "Street name": {no: "Gatenavn", pl: "Ulica"},
    "Enter the address here": {no: "Skriv inn adresse her", pl: "Podaj swój adres"},
    "Price for cleaning": {no: "Pris for rengjøring", pl: "Cena za sprzątanie"},
    "Price for carpet cleaning": {no: "Pris for tepperens", pl: "Cena za czyszczenie dywanu"},
    "Frequency": {no: 'Hyppighet', pl: 'Częstotliwość'},
    "Search": {no: "Søke", pl: "Szukaj"},
    "Show menu": {pl: "Pokaż menu", no: "Vis meny"},
    "Hide menu": {pl: "Schowaj menu", no: "Skjul menyen"},
    "Ordering": {no: "Bestilling", pl: "Zamawianie",},
    "Back": {pl: "Wróć", no: "Tilbake"},
    "Next": {pl: "Dalej", no: "Neste"},
    "Travel / cleaning products": {
        no: "Reise / renholdsprodukter",
        pl: "Podróż do klientów / środki czystości"
    },
    // Step 2
    "Maid service": {no: "Vaskehjelp", pl: "Sprzątanie"},
    "Cleanup after moving": {no: "Flyttevask", pl: "Sprzątanie po przeprowadzce"},
    "Furniture and carpet cleaning": {
        pl: 'Czyszczenie dywanu i mebli',
        no: "Møbel og tepperens",
    },
    "Determine the area": {no: "Bestem arealet", pl: "Określ obszar"},
    "Change the recommended time": {
        no: "Endre anbefalt tid",
        pl: "Zmień zalecany czas"
    },
    "What does a standard cleaning service include?": {
        pl: "Co zawierają standardowe usługi?",
        no: "Hva inkluderer en standard rengjøringstjeneste?"
    },
    "key.standard.service.equipment": {
        en: "We bring all necessary equipment and detergents with us: mops, microfiber cloths, vacuum cleaners and cleaning products.",
        no: "Vi tar med alt av nødvendig utstyr og vaskemidler med oss: mopper, mikrofiberkluter, støvsuger og rengjøringsmidler.",
        pl: "Cały niezbędny sprzęt i środki czystości przywozimy ze sobą: mopy, ściereczki z mikrofibry, odkurzacze i środki czystości."
    },
    "Specify the floor area": {pl: "Wprowadź powierzchnię", no: "Spesifiser gulvområdet"},
    "Add additional options": {pl: "Dodaj dodatkowe usługi", no: "Legg til flere alternativer"},
    "Select additional services": {no: "Velg ekstra tjenester", pl: "Wybierz dodatkowe opcje"},
    "Furniture cleaning": {no: "Møbelrens", pl: "Czyszczenie mebli"},

    // Step 2 - After Moving
    "Standard cleanup after moving includes": {
        no: "Standard Flyttevask inkluderer",
        pl: "Standardowe Sprzątanie po przeprowadzce zawiera"
    },
    "Doors": {pl: "Drzwi", no: "Dører"},
    "Washing of doors and moldings": {pl: "Mycie drzwi i listew", no: "Vask av dører og listverk"},
    "Windows": {pl: "Okna", no: "Vinduer"},
    "Window cleaning, exterior and interior": {
        pl: "Mycie okien, zewnętrzne i wewnętrzne",
        no: "Vindusvask, utvendig og innvendig"
    },
    "Floor": {pl: "Podłoga", no: "Gulv"},
    "Cleaning all floors": {pl: "Czyszczenie wszystkich podłóg", no: "Rengjøring av alle gulv"},
    "Kitchen": {pl: "Kuchnia", no: "Kjøkken"},
    "Kitchen interior, inside and outside sink": {
        pl: "Wnętrze kuchni, zlew wewnętrzny i zewnętrzny",
        no: "Kjøkkeninnredning, innvendig og utvendig vask"
    },
    "Cooker hood": {pl: "Okap", no: "Avtrekksvifte"},
    "Cleaning the fan in the kitchen": {
        pl: "Czyszczenie okapu w kuchni",
        no: "Rengjøring av ventilator på kjøkken"
    },
    "Drains": {pl: "Odpływ", no: "Sluk"},
    "Cleaning the drain": {pl: "Czyszczenie odpływu", no: "Rengjøring av sluk"},
    "Bathroom": {pl: "Łazienka", no: "Toalettrom"},
    "Toilet, bath, shower and under tub": {
        pl: "Toaleta, wanna, prysznic i pod wanną",
        no: "WC, bad, dusj og under badekar"
    },
    "Cabinets and drawers": {pl: "Komody i szuflady", no: "Skap og skuffer"},
    "Interior and exterior wash": {pl: "Mycie wewnątrz i na zewnątrz", no: "Innvendig og utvendig vask"},
    "Fixtures": {pl: "Osprzęt", no: "Lamper"},
    "Cleaning all lamps / luminaires": {
        pl: "Czyszczenie wszystkich lamp / opraw",
        no: "Rengjøring av alle lamper/armaturer"
    },
    "Roofs and walls": {pl: "Dach i ściany", no: "Tak og vegger"},
    "Wall and ceiling vacuumed": {pl: "Odkurzanie ścian i sufitu", no: "Vegg og tak støvsuges"},

    // Step 2 - Additional options
    "Window Cleaning": {no: "Vinduvask", pl: "Czyszczenie okien"},
    "Linen Change": {no: "Sengetøyskift", pl: "Zmiana pościeli"},
    "Waste containers": {no: "Avfallsbeholdere", pl: "Pojemniki na odpady"},
    "Appliances": {no: "Hvitevarer", pl: "Urządzenia AGD"},
    "Wardrobe": {no: "Garderobe", pl: "Szafa"},
    "Kitchen Fronts": {no: "Kjøkkenfronter", pl: "Fronty kuchenne"},
    "Ironing": {no: "Stryke", pl: "Prasowanie"},
    "Stoves / Fireplaces": {no: "Ovner / peiser", pl: "Piece / kominki"},
    "Blinds": {no: "Persienner", pl: "Rolety"},

    "The price is based on a standard window size": {
        no: "Prisen er basert på en standard størrelse",
        pl: "Cena jest oparta o standardowy rozmiar"
    },
    "Vacuuming of mattresses and linen change": {
        no: "Støvsuging av madrass og bytting av sengetøy",
        pl: "Odkurzanie materaców i zmiana pościeli"
    },
    "Emptying and washing to remove odor": {
        no: "Tømming og vasking for å fjerne lukt",
        pl: "Wynoszenie śmieci i mycie w celu usunięcia nieprzyjemnego zapachu"
    },
    "Washing the oven, refrigerator, etc. (Price per pcs.)": {
        no: "Vasking av ovn, kjøleskap, etc. (Pris pr. stk.)",
        pl: "Mycie piekarnika, lodówki, etc. (Cena za szt.)"
    },
    "Cleaning inside the wardrobe": {
        no: "Garderobe innvendig vask",
        pl: "Sprzątanie wewnątrz garderoby"
    },
    "Washing of kitchen fronts - exterior and interior": {
        no: "Vask av kjøkkenfronter - utvendig og innvendig",
        pl: "Mycie frontów kuchennych - zewnętrzne i wewnętrzne"
    },
    "Cleaning dust with brush (Price per pcs.)": {
        pl: "Czyszczenie kurzu za pomocą szczotki (Cena za szt.)",
        no: "Tørking av støv med børste (Pris pr. stk.)"
    },
    "Choose how many clothes you need:": {
        no: "Velg hvor mange klær du trenger:",
        pl: "Wybierz, ile potrzebujesz ubrań:"
    },
    "Oven door is washed, ash trays are emptied": {
        no: "Brannmur vaskes, askeskuffer tømmes",
        pl: "Myte są drzwiczki, popielniczki opróżniane"
    },
    'The service is available only if the customer orders surface cleaning.': {
        no: 'Tjenesten er bare tilgjengelig, Når en kunde bestiller vaskeoppdrag.',
        pl: 'Usługa jest dostępna tylko jeśli klient zamówi sprzątanie powierzchni.'
    },
    'Appendix 150NOK - for a one-time order, if the customer did not order a cleaning service.': {
        no: 'Tillegg 150NOK - for engangs oppdrag, Når en kunde ikke bestiller fast vaskeoppdrag.',
        pl: 'Dodatek 150NOK - za jednorazowe zlecenie, jeśli klient nie zamówił usługi sprzątania.'
    },
    "By choosing this service the customer should prepare an iron and ironing board.": {
        pl: "Wybierając tę usługę, klient powinien przygotować żelazko i deskę do prasowania.",
        no: "Ved å velge denne tjenesten kunden skal forberede et strykejern og strykebrett."
    },
    "Discount code": {pl: "Kod rabatowy", no: "Rabattkode"},
    "Sorry! This doesn't look like a valid discount code.": {
        pl: "Przykro nam! To nie wygląda jak poprawny kod rabatowy.",
    },
    "Success! Thank you for using a discount code!": {
        pl: "Dziękujemy! Przyznano kod zniżkowy!",
    },

    // Step 3 - Availability
    "Select the desired date and time below": {
        pl: "Wybierz dogodną datę i czas",
        no: "Velg ønsket dato og tid nedenfor"
    },
    "Unfortunately, we do not work in your area yet.": {
        pl: "Niestety, nie pracujemy jeszcze w tej okolicy",
        no: "Dessverre jobber vi ikke i ditt område ennå."
    },
    "key.availability.notificationPrompt": {
        pl: "Podaj nam swój adres e-mail, a poinformujemy Cię, kiedy rozpoczniemy pracę w Twojej okolicy. Wraz z informacją otrzymasz od nas rabat!",
        en: "Leave us your email address and we will inform you when we start working in your area. Along with information, you will receive a discount from us!",
        no: "Gi oss din e-postadresse og vi vil informere deg når vi begynner å jobbe i ditt område. Sammen med informasjon vil du motta en rabatt fra oss!"
    },
    "Thank you, we will inform you about availability in your area.": {
        pl: "Dziękujemy, poinformujemy Cię o dostępności w Twojej okolicy.",
    },
    "Notify": {
        pl: "Wyślij",
        no: "Sende",
    },
    "We're sorry, but this e-mail address looks incorrect.": {
        pl: "Przepraszamy, ale ten adres e-mail wygląda na nieprawidłowy.",
    },
    "Only once": {pl: "Tylko raz", no: "Kun en gang"},
    "Every week": {pl: "Co tydzień", no: "Hver uke"},
    "Twice a week": {pl: "Co dwa tygodnie", no: "Annenhver uke"},
    "Every month": {pl: "Co miesiąc", no: "Hver fjerde uke"},
    "Cleaning will take about {minutes} minutes.": {
        pl: "Sprzątanie zajmie około {minutes} minut.",
        no: "Rengjøring vil ta ca. {minutes} minutter.",
    },
    "{minutes} minutes longer": {
        pl: "{minutes} minut dłużej",
        no: "{minutes} minutter lenger"
    },
    "{minutes} minutes less": {
        pl: "{minutes} minut krócej",
        no: "{minutes} minutter kortere"
    },
    "Monday": {pl: "Poniedziałek", no: "Mandag"},
    "Tuesday": {pl: "Wtorek", no: "Tirsdag"},
    "Wednesday": {pl: "Środa", no: "Onsdag"},
    "Thursday": {pl: "Czwartek", no: "Torsdag"},
    "Friday": {pl: "Piątek", no: "Fredag"},
    "Saturday": {pl: "Sobota", no: "Lørdag"},
    "Sunday": {pl: "Niedziela", no: "Søndag"},
    "Do you have a discount code?": {pl: "Masz kod zniżkowy?", no: "Har du en rabattkode?"},
    "User comments": {pl: "Komentarze użytkowników", no: "Bruker kommentarer"},
    "Additional services": {pl: "Dodatkowe usługi", no: "Andre tjenester"},
    "Adjust cleaning time": {
        pl: "Dostosuj czas sprzątania",
        no: "Juster rengjøringen"
    },
    "Standard time": {
        pl: "Czas standardowy",
        no: "Standard tid",
    },
    "Time correction": {pl: "Korekta czasu", no: "Tidskorreksjon"},
    "Choose, if you have cleaning products and cleaning equipment": {
        pl: "Wybierz, czy masz środki czyszczące i sprzęt do czyszczenia",
        no: "Velg om du har rengjøringsmidler og renholdsutstyr",
    },
    "I have cleaning products": {
        no: "Jeg har rengjøringsmidler",
        pl: "Posiadam środki czystości"
    },
    "I have no cleaning products": {
        no: "Jeg har ingen rengjøringsmidler",
        pl: "Nie posiadam środków czystości"
    },
    "Attention": {
        no: "Obs!",
        pl: "Uwaga"
    },
    "You have already chosen the minimum cleaning time for this area.": {
        no: "Du har allerede valgt minimum rengjøringstid for denne overflaten.",
        pl: "Wybrałeś już minimalny czas sprzątania dla tej powierzchni."
    },
    "Total time": {pl: "Całkowity czas", no: "Total tid"},
    "Total to pay": {pl: "Całkowity koszt", no: "Til sammen å betale"},
    "Discount": {pl: "Rabat", no: "Rabatt"},
    "Credit cards vendors": {
        pl: "Dostawcy kart kredytowych",
        no: "Kredittkortleverandører"
    },
    "key.payment.security": {
        en: "Your personal information is protected by us, securely encrypted for your safety.",
        no: "Dine personopplysninger er beskyttet hos oss, kryptert på en trygg måte for din sikkerhet.",
        pl: "Twoje dane osobowe są przez nas chronione i zaszyfrowane dla Twojego bezpieczeństwa."
    },
    "To be chosen": {pl: "Do wybrania", no: "Å bli valgt"},
    "Use": {pl: "Użyj", no: "Bruk"},
    "Will you be home at the time of cleaning?": {
        pl: "Czy będziesz w domu podczas sprzątania?",
        no: "Skal du være hjemme på rengjøringstidpunktet?"
    },
    "Yes": {pl: "Tak", no: "Ja"},
    "No": {pl: "Nie", no: "Nei"},
    "You have reached the maximum allowed time for your area.": {
        no: "Du har allerede valgt maksimum rengjøringstid for denne overflaten.",
        pl: "Wybrałeś maksymalny dodatkowy czas dla tego rozmiaru."
    },
    "Contact information": {
        no: "Kontaktinformasjon",
        pl: "Informacje kontaktowe"
    },
    "Give us your contact details": {
        pl: "Jak możemy się z Tobą skontaktować?",
        no: "Gi oss kontaktinformasjonen din"
    },
    "Fill out the form and we will contact you within 24 hours.": {
        pl: "Wypełnij formularz, a skontaktujemy się z Tobą w ciągu 24 godzin.",
        no: "Fyll ut skjemaet, så kontakter vi deg innen 24 timer."
    },
    "First name": {no: "Fornavn", pl: "Imię"},
    "First and last name": {
        no: "Fornavn og etternavn",
        pl: "Imię i nazwisko"
    },
    "Send request": {
        no: "Send forespørsel",
        pl: "Czekam na kontakt"
    },
    "Surname": {no: "Etternavn", pl: "Nazwisko"},
    "E-mail address": {no: "E-postadresse", pl: "Adres e-mail"},
    "Phone number": {no: "Telefon", pl: "Numer telefonu"},
    "Travel allowance": {
        pl: "Dodatek za dojazd",
        no: "Kjøretillegg"
    },
    "Card fee": {
        no: "Kortavgift",
        pl: "Opłata za kartę"
    },
    "Please, type a correct first name.": {
        no: "Vennligst skriv inn et riktig fornavn.",
        pl: "Proszę, podaj poprawne imię."
    },
    "Please, type a correct surname.": {
        no: "Vennligst skriv inn et riktig etternavn.",
        pl: "Proszę, podaj poprawne nazwisko."
    },
    "Please, type a correct e-mail address.": {
        no: "Vennligst skriv inn en korrekt e-postadresse.",
        pl: "Proszę, podaj poprawny adres e-mail."
    },
    "Please, type a correct phone number.": {
        no: "Vennligst skriv inn et riktig telefonnummer.",
        pl: "Proszę, podaj poprawny numer telefonu."
    },
    "I will be present at the apartment, when the service arrives": {
        pl: "Będę obecny w mieszkaniu, gdy przybędzie usługa",
        no: "Jeg vil være til stede i leiligheten når tjenesten kommer"
    },
    "Provide a description of how we can gain access to your home": {
        pl: "Umieść opis, w jaki sposób uzyskamy dostęp do Twojego domu",
        no: "Gi en beskrivelse av hvordan vi får tilgang til hjemmet ditt"
    },
    "key.payment.houseAccess.placeholder": {
        no: '"Vi har en kodelås" eller "nøkkelen er på et trygt sted utenfor".',
        en: '"We have a code lock" or "the key is in a safe place outside".',
        pl: '"Mamy zamek na kod" lub "Klucz znajduje się w bezpiecznym miejscu na zewnątrz".'
    },
    "key.payment.purchaseAgreement": {
        pl: "Dokonując zakupu, akceptuję warunki zakupu i udzielam zgody na przetwarzanie moich danych firmie It Renhold.",
        en: "By completing the purchase, I accept the terms of purchase and give permission to process my data to It Renhold.",
        no: "Ved å fullføre kjøp aksepterer jeg kjøpsvilkår og godkjenner jeg It Renhold behandling av personopplysninger.",
    },
    "Name on the card": {pl: "Nazwisko na karcie", no: "Navn på kort"},
    "Card Number": {pl: "Numer karty", no: "Kortnummer"},
    "Expiration Date": {pl: "Data wygaśnięcia", no: "Utløpsdato"},
    "Security code": {pl: "Kod bezpieczeństwa", no: "Sikkerhetskode"},
    "Other information": {
        pl: "Inne informacje",
        no: "Andre opplysninger",
    },
    "We do not store your payment information.": {
        pl: "Nie przechowujemy informacji o płatnościach.",
        no: "Vi lagrer ikke betalingsinformasjonen din."
    },
    "Payment": {pl: "Płatność", no: "Innbetaling"},
    "Pay Now": {no: "Betal NÅ", pl: "Zapłać teraz"},
    "Secure SSL Encryption": {
        pl: "Bezpieczna transmisja SSL",
        no: "Sikker SSL-kryptering",
    },
    "Credit card": {no: "Kredittkort", pl: "Karta kredytowa"},
    "Pay with PayPal": {pl: "Zapłać PayPal'em", no: "Betal med PayPal"},
    "Invoice payment": {pl: "Wystaw fakturę", no: "Faktura betalning"},
    "Pay with a credit card": {pl: "Zapłać kartą kredytową", no: "Betal med kredittkort"},
    "Credit card number": {
        no: "Kreditt kort nummer",
        pl: "Numer karty kredytowej"
    },
    "Receive invoice by email when the service is performed": {
        pl: "Otrzymaj fakturę e-mailem po wykonaniu usługi.",
        no: "Motta faktura på epost når tjenesten er utført",
    },
    "key.invoice.summary": {
        en: "The invoice is sent electronically (as a PDF) to the E-mail address you have provided in the order with a payment deadline of 7 days.",
        no: 'Fakturaen blir sendt elektronisk (som en PDF) til den E-postadressen du har oppgitt i bestillingen med en betalingsfrist på 7 dager.',
        pl: 'Faktura jest wysyłana drogą elektroniczną (w formacie PDF) na adres e-mail podany w zamówieniu z terminem płatności 7 dni.'
    },
    "key.invoice.phoneVerification": {
        en: 'We will send a text message (SMS) with the authorization code to your phone. Enter the authorization code in the field and click "Continue" to confirm the order.',
        no: 'Vi sendes en tekstmelding (SMS) med autorisasjonskoden til telefonen din. Skriv inn autorisasjonskoden i feltet og klikk på "Fortsett" for å bekrefte bestillingen.',
        pl: 'Na Twój telefon wyślemy wiadomość tekstową (SMS) z kodem autoryzacyjnym. Wpisz kod autoryzacyjny w pole tekstowe i kliknij „Kontynuuj”, aby potwierdzić zamówienie.'
    },
    "Verification code": {pl: "Kod weryfikacyjny", no: "Bekreftelseskode"},
    "Send": {pl: "Wyślij", no: "Send"},
    "Continue": {pl: "Kontynuuj", no: "Fortsett",},
    "There was an error, with the data you submitted. Please, check your form for errors.": {
        pl: "Wystąpił błąd w przesłanych danych. Sprawdź, czy w formularzu nie ma błędów.",
        no: "Det oppsto en feil med dataene du sendte inn. Vennligst sjekk skjemaet ditt for feil."
    },
    "Please, wait, until you can request verification code again.": {
        pl: "Poczekaj, aż będziesz mógł ponownie zażądać kodu weryfikacyjnego.",
        no: "Vent til du kan be om bekreftelseskode igjen."
    },
    "Send verification SMS to:": {
        pl: "Wyślij potwierdzającego SMS na:",
        no: "Send bekreftelses-SMS til:"
    },
    "This code isn't your verification code.": {
        pl: "Ten kod nie jest Twoim kodem weryfikacyjnym.",
        no: "Denne koden er ikke bekreftelseskoden din.",
    },
    "Your code has been sent successfully, please check your phone.": {
        pl: "Twój kod został pomyślnie wysłany, sprawdź swój telefon.",
        no: "Koden din ble sendt, sjekk telefonen din.",
    },
    "Your phone number has been verified!": {
        pl: "Twój numer telefonu został zweryfikowany!",
        no: "Telefonnummeret ditt er bekreftet!",
    },
    "key.paste.invalidPhone": {
        en: "Your clipboard doesn't contain a valid phone number.",
        pl: "Twój schowek nie zawiera poprawnego numeru telefonu.",
        no: "Utklippstavlen inneholder ikke et gyldig telefonnummer."
    },
    "Cardholder name and surname": {
        pl: "Imię i nazwisko posiadacza karty",
        no: "Kortholderens navn og etternavn"
    },
    "Month": {pl: "Miesiąc", no: "Måned"},
    "Year": {pl: "Rok", no: "År"},

    'vue-phone-number-input.countrySelectorLabel': {
        pl: 'Wybierz kraj',
        no: 'Land',
        en: 'Country',
    },
    'vue-phone-number-input.countrySelectorError': {
        pl: '',
        no: '',
        en: '',
    },
    'vue-phone-number-input.phoneNumberLabel': {
        pl: ' ',
        no: ' ',
        en: ' ',
    },
    'vue-phone-number-input.example': {
        pl: 'Np:',
        no: 'Eks:',
        en: 'Eg.:',
    },
    "An error occurred while loading hours. Check your internet connection and try again.": {
        pl: "Wystąpił błąd podczas ładowania godzin. Sprawdź połączenie z internetem i spróbuj ponownie.",
        no: "Det oppsto en feil under belastningstiden. Sjekk Internett-tilkoblingen din og prøv igjen."
    },
    "An error occurred while loading data. Check your internet connection and try again.": {
        pl: "Wystąpił błąd podczas ładowania danych. Sprawdź połączenie z internetem i spróbuj ponownie.",
        no: "Det oppsto en feil under innlasting av data. Sjekk internettforbindelsen og prøv igjen."
    },
    "We had trouble gathering your location information.": {
        pl: "Mieliśmy problem z wczytaniem Twojej lokalizacji.",
        no: "Vi hadde problemer med å samle posisjonsinformasjonen din.",
    },
    "Please, fill all the necessary fields first.": {
        pl: "Proszę, wypełnij najpierw wszystkie wymagane pola.",
        no: "Vennligst fyll ut alle nødvendige felt først.",
    },
    "Please, type a correct name.": {
        no: "Vennligst skriv inn et riktig navn.",
        pl: "Proszę, wpisz poprawną nazwę."
    },
    "We're sorry! We couldn't proceed with PayPal payment!": {
        pl: "Przykro nam! Nie mogliśmy kontynuować płatności PayPal!",
        no: "We're sorry! We couldn't proceed with PayPal payment!"
    },
    "key.swipe.upper": {
        en: "Swipe left or right",
        pl: "Przesuń w lewo lub",
        no: "Sveip mot venstre eller",
    },
    "key.swipe.lower": {
        en: "in the calendar",
        pl: "w prawo w kalendarzu",
        no: "hoyre i kalenderen",
    }
};
