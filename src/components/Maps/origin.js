const ORIGIN_AUTOCOMPLETE = 'autocomplete';
const ORIGIN_CLEARED = 'cleared';
const ORIGIN_DEFAULT = 'default';
const ORIGIN_EXPLICIT = 'explicit';
const ORIGIN_GEOCODE = 'geocode';
const ORIGIN_LOCAL_STORAGE = 'localStorage';
const ORIGIN_PENDING = 'pending';

function isUserSelected(origin) {
    return [ORIGIN_EXPLICIT, ORIGIN_AUTOCOMPLETE, ORIGIN_GEOCODE].includes(origin);
}

function isImplicit(origin) {
    return [ORIGIN_DEFAULT, ORIGIN_GEOCODE, ORIGIN_LOCAL_STORAGE, ORIGIN_CLEARED].includes(origin);
}

const implicitTranslations = {
    [ORIGIN_AUTOCOMPLETE]: 'key.origin.autocomplete',
    [ORIGIN_CLEARED]: 'key.origin.cleared',
    [ORIGIN_DEFAULT]: 'key.origin.default',
    [ORIGIN_EXPLICIT]: 'key.origin.explicit',
    [ORIGIN_GEOCODE]: 'key.origin.geocode',
    [ORIGIN_LOCAL_STORAGE]: 'key.origin.localStorage',
};

export {
    ORIGIN_AUTOCOMPLETE,
    ORIGIN_CLEARED,
    ORIGIN_DEFAULT,
    ORIGIN_EXPLICIT,
    ORIGIN_GEOCODE,
    ORIGIN_LOCAL_STORAGE,
    ORIGIN_PENDING,
    isUserSelected,
    isImplicit,
    implicitTranslations
};
