const host = process.env.VUE_APP_AJAX_BACKEND_HOST || 'itrenhold.no';
const uri = path => `https://${host}${path}`;
const base = route => uri(`/Bestill_na_old/${route}`);
const api = route => uri(`/AP/api/${route}`);
const api2 = route => uri(`/AP/api/v2/${route}`);

export {base, api, api2};
