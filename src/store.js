import Vue from "vue";
import Vuex from 'vuex';
import axios from 'axios';
import {api, api2, base} from './api';
import {getPriceAbsolute, getPricePerHour} from './prices';
import {isUserSelected, ORIGIN_CLEARED, ORIGIN_PENDING} from "./components/Maps/origin";
import {emailValidator, formatPhoneNumber, isValidPhoneNumber, objectToFormData, sum, unicodeLetters} from './utils';
import {parseHourToMinute} from "@/time";

Vue.use(Vuex);

const priceAbsAndTimeByState = ({area, breakpoints}) => {
    if (area === 0) {
        return {price: 0, time: 0};
    }
    if (breakpoints != null) {
        return getPriceAbsolute(breakpoints, area);
    }
    return {price: 0, time: 0};
};

const pricePerHourAndTimeByState = ({area, breakpoints}) => {
    if (area === 0) {
        return {price: 0, time: 0};
    }
    if (breakpoints != null) {
        return getPricePerHour(breakpoints, area);
    }
    return {price: 0, time: 0};
};

export default new Vuex.Store({
    state: {
        // Step 1
        location: null,
        countryCode: null,
        countryName: null,
        autocompleteValue: '',
        locationOrigin: ORIGIN_PENDING,

        // Step 2
        type: null,
        equipment: null,
        equipmentPrice: null,
        breakpoints: {
            standard: null,
            extra: null,
        },
        serviceAreaStandard: 0,
        serviceAreaExtra: 0,
        carpetArea: 0,
        additionalServices: {
            standard: [],
            extra: [],
            gloves: [],
            carpet: []
        },
        timeCorrection: 0,
        transportPrice: 150,

        // Step 3
        serviceTime: {
            date: null,
            hour: null,
            employee_id: null,
        },
        repetition: 'never',

        // Step 4
        contact: {
            firstName: null,
            lastName: null,
            email: null,
            countryCode: 'NO',
            phoneNumber: null,
            accessInformation: null,
            accessInformationNeeded: null,
            agreement: false,
            cardHolderName: null
        },
        paymentMethod: null,
        creditCardInformation: {
            name: null,
            number: null,
            expirationMonth: null,
            expirationYear: null,
            cvv: null
        },
        previouslyVisitedSteps: {
            "/": false,
            "/details": false,
            "/availability": false,
            "/payment": false,
        },
        cardFee: 30,
        cardFeeVisible: false,
        invoicePhoneNumberVerified: false,
        invoicePhoneNumber: null,
        timerStart: null,
        timerCounting: false,

        // Sidebar
        discount: {
            code: '',
            procent: 0,
        }
    },

    getters: {
        formattedPhoneNumber: state => {
            try {
                return formatPhoneNumber(state.contact.phoneNumber, state.contact.countryCode);
            } catch (e) {
                return state.contact.phoneNumber;
            }
        },
        incompleteSteps: (state, getters) => {
            const validSteps = getters.validSteps;

            return {
                "/": state.previouslyVisitedSteps["/"] ? !validSteps['/'] : null,
                "/details": state.previouslyVisitedSteps["/details"] ? !validSteps["/details"] : null,
                "/availability": state.previouslyVisitedSteps["/availability"] ? !validSteps["/availability"] : null,
                "/payment": state.previouslyVisitedSteps["/payment"] ? !validSteps["/payment"] : null,
            };
        },
        allStepsValid: (state, getters) => {
            return getters.stepLocationValid &&
                getters.stepDetailsValid &&
                getters.stepCalendarValid &&
                getters.stepPaymentValid;
        },
        stepLocationValid: state => state.countryCode === "NO" && isUserSelected(state.locationOrigin),
        stepDetailsValid: (state, getters) => {
            function anyHasValue(array) {
                const anyThatHasValue = array.find(service => service.value && service.value > 0);
                return !!anyThatHasValue;
            }

            switch (state.type) {
                case "standard":
                    return state.serviceAreaStandard > 0 && !getters.overtime && state.equipment !== null;
                case "extra":
                    return state.serviceAreaExtra > 0 && !getters.overtime;
                case "gloves":
                    return anyHasValue(state.additionalServices.gloves);
                case "carpet":
                    return state.carpetArea > 0;
                default:
                    return false;
            }
        },
        stepCalendarValid: state => !!state.serviceTime.date,
        stepPaymentValid: state => {
            const contact = state.contact;
            const firstName = contact.firstName && unicodeLetters(contact.firstName);
            const lastName = contact.lastName && unicodeLetters(contact.lastName);
            const cardHolderName = contact.lastName && unicodeLetters(contact.cardHolderName);
            const email = contact.email && emailValidator(contact.email);
            const phone = contact.phoneNumber && isValidPhoneNumber(contact.phoneNumber, contact.countryCode) === true;
            return firstName && lastName && email && phone && (state.paymentMethod !== 'stripe' || (contact.agreement && cardHolderName));
        },
        validSteps: (state, getters) => {
            return {
                "/": getters.stepLocationValid,
                "/details": getters.stepDetailsValid,
                "/availability": getters.stepCalendarValid,
                "/payment": getters.stepPaymentValid,
            };
        },
        overtime: (state, getters) => getters.timeInMinutes > 12 * 60,
        timeCorrection({type, timeCorrection}) {
            if (type === 'standard') {
                return timeCorrection;
            }
            return 0;
        },
        timeCorrectionPrice: (state, getters) => getters.timeCorrection / 30 * 179,

        timeInMinutes: (state, getters) => {
            const estimatedTime = getters.floorTime + getters.additionalTime;
            if (estimatedTime === 0) {
                return 0;
            }
            return estimatedTime + getters.timeCorrection;
        },
        price: (state, getters) => {
            const fractional = (getters.floorPrice + getters.additionalPrice + getters.timeCorrectionPrice) * (100 - state.discount.procent) / 100.0 + getters.equipmentPrice;
            let basePrice = Math.ceil(fractional);
            if (state.cardFeeVisible) {
                basePrice += state.cardFee;
            }
            if (getters.includesTransportPrice) {
                basePrice += state.transportPrice;
            }
            return basePrice;
        },

        floorPriceAndTime: ({type, serviceAreaStandard, serviceAreaExtra, breakpoints}, {toBeContactedPersonally}) => {
            if (type === "standard") {
                return pricePerHourAndTimeByState({area: serviceAreaStandard, breakpoints: breakpoints[type]});
            }
            const area = serviceAreaExtra;
            if (toBeContactedPersonally) {
                return {price: null, time: null};
            }
            return priceAbsAndTimeByState({area: area, breakpoints: breakpoints[type]});
        },
        floorPrice: (state, getters) => getters.floorPriceAndTime.price,
        floorTime: (state, getters) => getters.floorPriceAndTime.time,
        carpetPrice: (state) => state.carpetArea * 14,

        additionalTime: state => {
            if (state.type === null) {
                return 0;
            }
            return state.additionalServices[state.type]
                .map(service => service.time * service.count)
                .reduce(sum, 0);
        },
        additionalPrice: state => {
            if (state.type === null) {
                return 0;
            }
            return state.additionalServices[state.type]
                .map(service => service.price * service.count)
                .reduce(sum, 0);
        },
        equipmentPriceVisible: (state, getters) => {
            return state.type === 'standard' && getters.equipmentPrice > 0;
        },
        equipmentPrice: state => {
            if (state.type === 'standard' && state.equipment === 'missing') {
                return state.equipmentPrice;
            }
            return 0;
        },
        periodicity: state => {
            return ['never', 'week', '2week', 'month'].indexOf(state.repetition);
        },
        isLocationAvailable: ({location, countryCode}) => {
            if (location === null) {
                return false;
            }
            const {lat, lng} = location;
            return lat && lng && (countryCode == null || countryCode === 'NO');
        },
        toBeContactedPersonally: ({type, serviceAreaExtra}) => type === 'extra' && serviceAreaExtra >= 199,
        timeVisible: ({type}) => type !== 'extra',
        summaryVisible: ({type}, {toBeContactedPersonally}) => {
            if (toBeContactedPersonally) {
                return false;
            }
            return type !== null;
        },
        includesTransportPrice: ({type}) => type === 'gloves',
        repetition: ({type, repetition}) => {
            if (type === 'standard') {
                return repetition;
            }
            return null;
        },

        orderData(state, getters) {
            const type = state.type;
            let removal; // this is some bizzare enum value, used to represent the type of the service
            if (type === "standard") {
                removal = 0;
            } else if (type === "extra") {
                removal = 1;
            } else {
                throw new Error("Type unexpected: " + type);
            }

            return {
                address: state.autocompleteValue,
                zip_code: '. ',
                city: '. ',
                lat: state.location.lat,
                lng: state.location.lng,

                price: getters.price,
                currency: 'USD',
                employee_id: state.serviceTime.employee_id,
                date: state.serviceTime.date,
                hour: state.serviceTime.hour,
                time: getters.floorTime,

                first_name: state.contact.firstName,
                last_name: state.contact.lastName,
                email: state.contact.email,
                number: getters.formattedPhoneNumber,

                additional: state.contact.accessInformation || '',
                additional_services: JSON.stringify({
                    additional_services: state.additionalServices[type]
                        .filter(option => option.count > 0)
                        .map(option => ({
                            id: option.id,
                            count: option.count
                        }))
                }),
                removal,
                key: state.discount.code,
                periodicity: getters.periodicity
            };
        },
    },

    mutations: {
        countryCode(state, {location, country, origin}) {
            state.location = location;
            state.countryCode = country.code;
            state.countryName = country.name;
            state.locationOrigin = origin;
        },
        clearCountryCode(state) {
            state.location = null;
            state.countryCode = null;
            state.countryName = null;
            state.locationOrigin = ORIGIN_CLEARED;
        },
        autocompleteValue(state, {value}) {
            state.autocompleteValue = value;
        },
        serviceType(state, {type}) {
            state.type = type;
        },
        equipment(state, {equipment}) {
            state.equipment = equipment;
        },
        timeCorrection(state, {correction}) {
            state.timeCorrection = correction;
        },
        flatBreakpoints(state, {type, breakpoints}) {
            state.breakpoints[type] = breakpoints;
        },
        equipmentPrice(state, {price}) {
            state.equipmentPrice = price;
        },
        serviceAreaStandard(state, {area}) {
            state.serviceAreaStandard = area;
            state.timeCorrection = 0;
        },
        serviceAreaExtra(state, {area}) {
            state.serviceAreaExtra = area;
            state.timeCorrection = 0;
        },
        carpetArea(state, {area}) {
            state.carpetArea = area;
        },
        additionalService(state, {forType, options}) {
            state.additionalServices[forType] = options;
        },
        savePaymentType(state, {method}) {
            state.paymentMethod = method;
        },
        date(state, {date, text, employee_id}) {
            state.serviceTime.date = date;
            state.serviceTime.hour = text;
            state.serviceTime.employee_id = employee_id;
        },
        leavePage(state, {step}) {
            state.previouslyVisitedSteps[step] = true;
        },
        enterPage(state, {step}) {
            switch (step) {
                case "/payment":
                    state.previouslyVisitedSteps["/availability"] = true;
                case "/availability":
                    state.previouslyVisitedSteps["/details"] = true;
                case "/details":
                    state.previouslyVisitedSteps["/"] = true;
            }
        },
        validateAll(state) {
            state.previouslyVisitedSteps["/payment"] = true;
            state.previouslyVisitedSteps["/availability"] = true;
            state.previouslyVisitedSteps["/details"] = true;
            state.previouslyVisitedSteps["/"] = true;
        },
        repetition(state, {repetition}) {
            state.repetition = repetition;
        },
        savePaymentInfo(state, {
            firstName,
            lastName,
            email,
            phoneNumber,
            countryCode,
            accessInformation,
            accessInformationNeeded,
            agreement,
            cardHolderName
        }) {
            state.contact.firstName = firstName;
            state.contact.lastName = lastName;
            state.contact.email = email;
            state.contact.phoneNumber = phoneNumber;
            state.contact.countryCode = countryCode;
            state.contact.accessInformation = accessInformation;
            state.contact.accessInformationNeeded = accessInformationNeeded;
            state.contact.agreement = agreement;
            state.contact.cardHolderName = cardHolderName;
        },
        creditCardInfo(state, {name, number, expirationMonth, expirationYear, cvv}) {
            state.creditCardInformation = {name, number, expirationMonth, expirationYear, cvv};
        },
        timerStartedAt(state, {timestamp}) {
            state.timerStart = timestamp;
            state.timerCounting = true;
        },
        timerStopped(state) {
            state.timerCounting = false;
        }
    },

    actions: {
        validateDiscount(context, {code}) {
            return new Promise((resolve, reject) => {
                axios.post(api('code/check'), {key: code})
                    .then(({data}) => {
                        if (data.code) {
                            context.state.discount.code = code;
                            context.state.discount.procent = data.code.procent;
                            resolve();
                            return;
                        }
                        reject("invalid");
                    })
                    .catch(error => {
                        context.state.discount.procent = 0;
                        if (error.response.status === 404) {
                            reject("missing");
                        } else {
                            reject("other");
                        }
                    });
            });
        },
        fetchComments() {
            return new Promise((resolve, reject) => {
                axios(api('comments/get_all'))
                    .then(({data}) => {
                        resolve(data);
                    })
                    .catch(() => {
                        reject();
                    });
            });
        },
        saveForNewsLetter(context, {email}) {
            return new Promise((resolve, reject) => {
                axios.post(api('maillist/create'), {email})
                    .then(() => {
                        resolve();
                    })
                    .catch(({response}) => {
                        reject({status: response.status});
                    });
            });
        },
        saveForAreaAvailability(context, {email}) {
            return new Promise((resolve, reject) => {
                axios.post(api('addEmailOrder'), {
                    email,
                    address: context.state.autocompleteValue,
                    lat: context.state.location.lat,
                    lng: context.state.location.lng,
                })
                    .then(() => resolve())
                    .catch(() => reject());
            });
        },
        saveContactRequest(context, {name, phone, email, other}) {
            return axios.post(api2('mail'), {name, phone, email, info: other});
        },
        fetchHours(context, {equipment}) {
            const requiresCar = context.state.type === 'extra';
            const time = context.getters.floorTime;
            const lat = context.state.location.lat;
            const lng = context.state.location.lng;

            return new Promise((resolve, reject) => {
                axios
                    .get(api2('days/hours'), {
                        params: {
                            time,
                            lat,
                            lng,
                            requires_car: requiresCar ? '1' : '0',
                            equipment
                        }
                    })
                    .then(({data}) => resolve(data))
                    .catch(reject);
            });
        },
        fetchServiceType(context, {type}) {
            const call = (url, type, mapper) => {
                return axios({
                    method: 'GET',
                    url: api2(url),
                    params: {serviceType: type}
                })
                    .then(({data}) => {
                        // This API is fucked up, I have no idea what moron designed it
                        const priceAndTime = data.areas.map(mapper);
                        context.commit('flatBreakpoints', {type, breakpoints: priceAndTime});
                        context.commit('equipmentPrice', {price: data.equipmentPrice});
                        return priceAndTime;
                    });
            };

            if (type === "standard") {
                return call('serviceType', type, ({price, area, min}) => ({
                    pricePerHour: price,
                    priceAbsolute: null,
                    area,
                    time: parseHourToMinute(min)
                }));
            }
            if (type === "extra") {
                return call('serviceType', type, ({price, area, min}) => ({
                    pricePerHour: null,
                    priceAbsolute: price,
                    area,
                    time: parseHourToMinute(min)
                }));
            }
            throw `Unexpected type: ${type}`;
        },
        payStripe(context, {stripe, card, token}) {
            const {state: {contact}, getters} = context;

            let stripeRequestData = {
                firstName: contact.firstName,
                lastName: contact.lastName,
                email: contact.email,
                phone: contact.phoneNumber,
                amount: getters.price,
                orderData: getters.orderData,
                creditCard: {name: contact.firstName + ' ' + contact.lastName}
            };
            return new Promise((resolve, reject) => {
                axios({
                    method: 'post',
                    url: base('getToken'),
                    headers: {'Content-Type': 'multipart/form-data'},
                    data: objectToFormData(stripeRequestData)
                })
                    .then(response => response.data)
                    .then(response => {
                        stripe
                            .confirmCardPayment(response.client_secret, {
                                payment_method: {card: card, billing_details: {name: response.cardName}}
                            })
                            .then(result => {
                                if (result.error) {
                                    reject(result.error);
                                } else {
                                    // The payment has been processed!
                                    if (result.paymentIntent.status === 'succeeded') {
                                        // Show a success message to your customer
                                        // There's a risk of the customer closing the window before callback
                                        // execution. Set up a webhook or plugin to listen for the
                                        // payment_intent.succeeded event that handles any business critical
                                        // post-payment actions.

                                        axios
                                            .post("https://itrenhold.no/Bestill_na_old/payment", objectToFormData({
                                                ...stripeRequestData,
                                                pay_id: result.paymentIntent.id
                                            }))
                                            .then(response => window.location.href = "https://itrenhold.no/slapp-av/")
                                            .catch(error => reject(error));
                                    }
                                }
                            })
                            .catch(error => reject(error));
                    })
                    .catch(error => reject(error));
            });
        },
        payPaypal(context) {
            return axios
                .post(api('order/store'), context.getters.orderData)
                .then(({data}) => data)
        },
        sendVerifyPhoneNumber(context, {phoneNumber, locale}) {
            return axios.post(api2('verification'), {phoneNumber, locale, orderData: context.getters.orderData});
        },
        verifyCode(context, {code}) {
            return axios.post(api2('verification/verify'), {verificationCode: code});
        }
    }
});
