import Vue from "vue";
import VueRouter from "vue-router";
import {i18n, Translate} from './plugins/i18n';
import App from "./App";
import routes from "./routes";
import store from "./store";
import {hour, signedValue} from "./filters";
import {onlyNumbers} from "./directives";
import Chartist from "chartist";
import SlimDialog from 'v-slim-dialog';
import VueTouchEvents from 'vue2-touch-events';

import * as VueGoogleMaps from 'vue2-google-maps';
import '../node_modules/bootstrap/scss/bootstrap.scss';
import 'v-slim-dialog/dist/v-slim-dialog.css';
import '@/assets/scss/global.scss';
import "es6-promise/auto";

import MobileMenuPlugin from './plugins/MobileMenuPlugin';
import Notifications from "./plugins/NotificationPlugin";
import FontAwesome from "./components/FontAwesome";

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCWlUs26-eUz0JURkuGLHy8_RJl0DBTLOo',
        language: localStorage.locale,
        libraries: 'places',
    },
});

const router = new VueRouter({
    mode: 'hash',
    base: 'order',
    routes,
    linkExactActiveClass: "nav-item active"
});

Vue.use(VueTouchEvents, {disableClick: true});
Vue.use(SlimDialog);
Vue.use(MobileMenuPlugin);
Vue.use(VueRouter);
Vue.use(Notifications);
Vue.directive('onlyNumbers', onlyNumbers);
Vue.component("fa", FontAwesome);
Vue.component('translate', Translate);
Vue.filter('hour', hour);
Vue.filter('signedValue', signedValue);

Vue.prototype.$Chartist = Chartist;

new Vue({
    el: "#app",
    render: h => h(App),
    router,
    store,
    i18n,
    data: {Chartist}
});
