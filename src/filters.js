function formatPositiveValue(value) {
    const minutes = value % 60;
    const hours = Math.floor(value / 60.0);
    return hours + ":" + addLeadingZero(minutes);
}

function addLeadingZero(value) {
    if (("" + value).length === 1) {
        return "0" + value;
    }
    return value;
}

function hour(minutes) {
    let formattedAbsValue = formatPositiveValue(Math.abs(minutes));
    if (minutes < 0) {
        return '- ' + formattedAbsValue;
    }
    return formattedAbsValue;
}

function signedValue(value) {
    if (value === "") {
        return "";
    }
    if (value[0] === "-") {
        return value;
    }
    return "+ " + value;
}

export {hour, signedValue};
