export function parseHourToMinute(hour) {
    if (typeof hour === "number") {
        return Math.round(hour * 60.0);
    }
    throw "Expected float time";
}
