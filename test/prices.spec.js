import {describe, it} from "mocha";
import {assert, expect} from 'chai';
import {getPricePerHour} from "@/prices";

describe("getPricePerHour()", () => {
    it("should get price", () => {
        // given
        let breakpoints = [
            {pricePerHour: 1800, area: 13},
            {pricePerHour: 1500, area: 11, time: 10},
            {pricePerHour: 1200, area: 9},
            {pricePerHour: 600, area: 7},
        ];

        // when
        const price = getPricePerHour(breakpoints, 10);

        // then
        assert.deepEqual(price, {price: 250, time: 10});
    });

    it("should get price (edge)", () => {
        // given
        let breakpoints = [
            {pricePerHour: 120, area: 13, time: 15},
            {pricePerHour: 100, area: 11},
            {pricePerHour: 80, area: 9},
        ];

        // when
        const price = getPricePerHour(breakpoints, 11);

        // then
        assert.deepEqual(price, {price: 30, time: 15});
    });

    it("should get price (low)", () => {
        // given
        let breakpoints = [{pricePerHour: 240, area: 9, time: 5}];

        // when
        const price = getPricePerHour(breakpoints, -2);

        // then
        assert.deepEqual(price, {price: 20, time: 5});
    });

    it("should throw (high)", () => {
        // given
        let breakpoints = [{price: 30, area: 13},];

        // when
        expect(() => getPricePerHour(breakpoints, 15)).to.throw('High price');
    });

    it("should throw for an empty array", () => {
        // when
        expect(() => getPricePerHour([], 10)).to.throw('Empty breakpoints');
    });

    it("should throw for null", () => {
        // when
        expect(() => getPricePerHour(null, 10)).to.throw('Invalid breakpoints');
    });
});
