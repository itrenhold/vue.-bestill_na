module.exports = {
    publicPath: "/Bestill_na",
    outputDir: "dist",
    indexPath: "index.html",
    configureWebpack: {
        output: {
            filename: "[name].[hash].js",
        }
    }
};
